from django.contrib import admin
from django.urls import path, include

# from .views import addActivity
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('add/',views.addActivity,name='addActivity'),
    path('list/', views.listActivity, name='listActivity'),
    path('register/<int:task_id>/', views.register, name='register'),
    path('delete/<int:delete_id>/', views.deleteUser, name='delete'),
]