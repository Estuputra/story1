from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Kegiatan, Orang
from django.apps import apps
from .views import index, addActivity, listActivity, deleteUser, register
from .forms import FormKegiatan, FormOrang
from .apps import Story6Config


class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi_kegiatan="belajar PPW")
        self.orang = Orang.objects.create(nama_orang="Estuputra")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "belajar")
        self.assertEqual(str(self.orang), "Estuputra")


class FormTest(TestCase):

    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "nama_kegiatan": "belajar",
            "deskripsi_kegiatan": "belajar PPW"
        })
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(data={
            'nama_orang': "Estuputra"
        })
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())


class UrlsTest(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi_kegiatan="belajar PPW")
        self.orang = Orang.objects.create(
            nama_orang="Joni", kegiatan=Kegiatan.objects.get(nama_kegiatan="belajar"))
        self.index = reverse("index")
        self.listActivity = reverse("listActivity")
        self.addActivity = reverse("addActivity")
        self.register = reverse("register", args=[self.kegiatan.pk])
        self.delete = reverse("delete", args=[self.orang.pk])

    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_listActivity_use_right_function(self):
        found = resolve(self.listActivity)
        self.assertEqual(found.func, listActivity)

    def test_addActivity_use_right_function(self):
        found = resolve(self.addActivity)
        self.assertEqual(found.func, addActivity)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, deleteUser)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("index")
        self.listActivity = reverse("listActivity")
        self.addActivity = reverse("addActivity")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/listActivity.html')

    def test_GET_addActivity(self):
        response = self.client.get(self.addActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/addactivity.html')

    def test_POST_addActivity(self):
        response = self.client.post(self.addActivity,
                                    {
                                        'nama_kegiatan': 'belajar',
                                        'deskripsi_kegiatan ': "belajar PPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addActivity_invalid(self):
        response = self.client.post(self.addActivity,
                                    {
                                        'nama_kegiatan': '',
                                        'deskripsi_kegiatan ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story6/addactivity.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan="CDF")
        kegiatan.save()
        orang = Orang(nama_orang="mangoleh",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan="CDF")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/register/1/',
                                 data={'nama_orang': 'bangjago'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/register/1/')
        self.assertTemplateUsed(response, 'story6/register.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/register/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'story6/register.html')
        # self.assertEqual(response.status_code, 302)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')
