from django.shortcuts import render
from datetime import datetime
# Create your views here.


def index(request):
    return render(request, 'homepage/index.html')


def landingPage(request):
    return render(request, 'homepage/page.html')


def comingSoon(request):
    return render(request, 'homepage/soon.html')
