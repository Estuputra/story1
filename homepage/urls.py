from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('', views.index),
    path('landing/', views.landingPage, name='landing'),
    path('comingSoon/', views.comingSoon, name='comingSoon'),

]
