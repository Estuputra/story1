from django import forms

class FormMatkul(forms.Form):
    nama_matkul = forms.CharField(
        label = "Nama mata kuliah", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    jumlah_sks = forms.IntegerField(
        label = "Jumlah sks",
        widget = forms.NumberInput(
            attrs={
                'class':'form-control',
                'min':'1',
                'max':'6'
            }
        )
    )

    ruangan = forms.CharField(
        label = "Ruangan", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    dosen = forms.CharField(
        label = "Nama dosen", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    deskripsi = forms.CharField(
        label = "Deskripsi mata kuliah", 
        max_length = 100,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )


    
