from django.urls import path
from . import views
urlpatterns = [
    path('',views.index,name='story5'),
    path('addMatkul/',views.addMatkul, name='addMatkul'),
    path('listMatkul/',views.listMatkul,name = 'listMatkul'),
    path('delete/P<int:delete_id>/',views.delete, name='delete'),
    path('details/P<int:matkul_id>',views.details, name='details'),

]
