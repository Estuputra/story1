from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length = 100)
    jumlah_sks = models.IntegerField()
    ruangan = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    deskripsi = models.CharField(max_length = 1000, null = True)

    def __str__(self):
        return self.nama_matkul


