from django.shortcuts import render ,redirect
from . import forms
from .models import MataKuliah
# Create your views here.

def index(request):
    return render(request,'story5/index.html')
def addMatkul(request):
    form_matkul = forms.FormMatkul()
    if request.method == 'POST':
        form_matkul_input = forms.FormMatkul(request.POST)
        if form_matkul_input.is_valid():
            data = form_matkul_input.cleaned_data
            matkul_input = MataKuliah()
            matkul_input.nama_matkul = data['nama_matkul']
            matkul_input.jumlah_sks = data['jumlah_sks']
            matkul_input.ruangan = data['ruangan']
            matkul_input.dosen = data['dosen']
            matkul_input.deskripsi = data['deskripsi']
            matkul_input.save()
            current_data = MataKuliah.objects.all()

            return redirect('story5')  
        else:
            current_data = MataKuliah.objects.all()
            return render(request, 'story5/addMatkul.html',{'form':form_matkul, 'status':'failed','data':current_data})
    else:
        current_data = MataKuliah.objects.all()
        return render(request, 'story5/addMatkul.html',{'form':form_matkul,'data':current_data})

def listMatkul(request):
    data = MataKuliah.objects.all()
    return render(request,'story5/listMatkul.html',{'listMatkul':data})

def delete(request, delete_id):
    try:
        jadwal_to_delete = MataKuliah.objects.get(pk = delete_id)
        jadwal_to_delete.delete()
        return redirect('listMatkul')
    except:
        return redirect('listMatkul')

def details(request, matkul_id):
    try:
        matkulToView = MataKuliah.objects.get(pk = matkul_id)
        context = {
            'matkul':matkulToView
        }
        return render(request, 'story5/detailMatkul.html',context)
    except:
        return redirect('listMatkul')   